const path = require('path');
const config = require('./webpack.config');

module.exports = {
    ...config,
    mode: 'production',
    devtool: 'source-map',
    output: {
        path: path.join(__dirname, 'dist'),
    }
};