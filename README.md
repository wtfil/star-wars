# Start Wars Demo

This is a demo of using [Star Wars API](https://swapi.co)

## Install

In order to install this demo you will need to have [NodeJS](https://nodejs.org/en/) and [yarn](https://yarnpkg.com/en/docs/install). If you already have this, simly run following command.

    yarn

## Start

    yarn start
    open http://127.0.0.1:8090

## Build

    yarn build

You can test your build by runing following commands

    yarn serve
    open http://127.0.0.1:8091
