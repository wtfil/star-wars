import {ActionType, AppCommand, Planet, Film} from './types';
import * as api from './api';

export type SearchStart = ActionType<'SearchStart', string>;
export type SearchSkip = ActionType<'SearchSkip', string>;
export type SearchSuccess = ActionType<'SearchSuccess', {data: api.SearchResponse, query: string}>;
export type SearchError = ActionType<'SearchError', Error>;

export const search = (query: string): AppCommand => async (dispatch, getState) => {
    const queryTimmed = query.trim();

    const {search} = getState();


    if (!queryTimmed || search.history.includes(queryTimmed)) {
        dispatch({type: 'SearchSkip', payload: query});
        return;
    }

    dispatch({type: 'SearchStart', payload: query});

    try {
        const data = await api.search(query);
        for (let i = 0; i < data.results.length; i ++) {
            const item = data.results[i];
            await Promise.all([
                dispatch(getPlanet(item.homeworld)),
                dispatch(duckSearch(item.name)),
                Promise.all(item.films.map(film =>
                    dispatch(getFilm(film)),
                ))
            ])
        }
        dispatch({type: 'SearchSuccess', payload: {query, data}});
    } catch (error) {
        dispatch({type: 'SearchError', error});
    }
}

export type GetPlanetStart = ActionType<'GetPlanetStart', string>;
export type GetPlanetSuccess = ActionType<'GetPlanetSuccess', Planet>;
export type GetPlanetError = ActionType<'GetPlanetError'>;

export const getPlanet = (url: string): AppCommand => async (dispatch, getState) => {
    const {planets} = getState();
    if (planets.loadingItems.includes(url)) {
        return;
    }
    dispatch({type: 'GetPlanetStart', payload: url});
    try {
        const data = await api.getPlanet(url);
        dispatch({type: 'GetPlanetSuccess', payload: data});
    } catch {
        dispatch({type: 'GetPlanetError'});
    }
}

export type DuckSearchStart = ActionType<'DuckSearchStart', string>
export type DuckSearchSuccess = ActionType<'DuckSearchSuccess', {query: string, data: api.DuckDuckGoSearchResponce}>
export type DuckSearchError = ActionType<'DuckSearchError', Error>

export const duckSearch = (query: string): AppCommand => async (dispatch, getState) => {
    const {images} = getState();
    if (query in images) {
        return;
    }
    dispatch({type: 'DuckSearchStart', payload: query});
    try {
        const data = await api.duckSearch(query);
        dispatch({type: 'DuckSearchSuccess', payload: {query, data}});
    } catch (e) {
        dispatch({type: 'DuckSearchError', error: e});
    }
}

export type GetFilmtStart = ActionType<'GetFilmStart', string>;
export type GetFilmtSuccess = ActionType<'GetFilmSuccess', {url: string, data: Film}>;
export type GetFilmtError = ActionType<'GetFilmError', Error>;

export const getFilm = (url: string): AppCommand => async (dispatch, getState) => {
    const {films} = getState();
    if (films.loadingItems.includes(url)) {
        return;
    }
    dispatch({type: 'GetFilmStart', payload: url});
    try {
        const data = await api.getFilm(url);
        dispatch({type: 'GetFilmSuccess', payload: {url, data}});
    } catch (e) {
        dispatch({type: 'GetPlanetError', error: e});
    }
}