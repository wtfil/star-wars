import {createStore as createReduxStore, combineReducers, applyMiddleware} from 'redux';
import thunk, {ThunkMiddleware} from 'redux-thunk';

import {State, AppAction} from './types';
import {characters, planets, search, images, films} from './reducer';

const reducer = combineReducers({
    characters,
    planets,
    search,
    images,
    films
});

const middlewares = [
    thunk as ThunkMiddleware<State, AppAction>,
];

if (process.env.NODE_ENV === 'development') {
    const {createLogger} = require('redux-logger');
    const logger = createLogger({
        diff: true,
        collapsed: true
    });
    middlewares.push(logger)
}

export const createStore = () => {
    return createReduxStore(
        reducer,
        applyMiddleware(...middlewares)
    );
};