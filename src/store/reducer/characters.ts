import {AppAction, State} from '../types';
import {arrayToHash} from '../utils';

const INITIAL_STATE = {}
type Characters = State['characters']

export const characters = (
    state: Characters = INITIAL_STATE,
    action: AppAction
) => {
    switch (action.type) {
        case 'SearchSuccess':
            return {
                ...state,
                ...arrayToHash(action.payload.data.results)
            }
        default:
            return state;
    }
}