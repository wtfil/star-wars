import {AppAction, State} from '../types';

type Search = State['search'];

const INITIAL_STATE: Search = {
    value: '',
    status: 'ready',
    history: []
}

export const search = (
    state: Search = INITIAL_STATE,
    action: AppAction
): Search => {
    switch (action.type) {
        case 'SearchSkip':
            return {
                ...state,
                value: action.payload
            }
        case 'SearchStart':
            return {
                ...state,
                status: 'loading',
                value: action.payload
            }
        case 'SearchSuccess':
            return {
                ...state,
                status: action.payload.query === state.value ?
                    'ready' : state.status,
                history: state.history.concat(action.payload.query.trim())
            }
        default:
            return state;
    }
}
