import {AppAction, State} from '../types';

const INITIAL_STATE = {};

export const images = (
    state: State['images'] = INITIAL_STATE,
    action: AppAction
) => {
    switch (action.type) {
        case 'DuckSearchStart':
            return {
                ...state,
                [action.payload]: ''
            };
        case 'DuckSearchSuccess':
            const {query, data} = action.payload;
            return {
                ...state,
                [query]: data.Image
            };
        default:
            return state;
    }
}