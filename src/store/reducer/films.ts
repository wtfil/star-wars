import {AppAction, State} from '../types';

const INITIAL_STATE = {
    loadingItems: [],
    data: {}
}

export const films = (
    state: State['films'] = INITIAL_STATE,
    action: AppAction
): State['films'] => {
    switch (action.type) {
        case 'GetFilmStart':
            return {
                ...state,
                loadingItems: state.loadingItems.concat(action.payload)
            }
        case 'GetFilmSuccess':
            return {
                ...state,
                data: {
                    ...state.data,
                    [action.payload.url]: action.payload.data
                }
            }
        default:
            return state;
    }
}