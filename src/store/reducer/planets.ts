import {AppAction, State} from '../types';

type Planets = State['planets'];

const INITIAL_STATE = {
    loadingItems: [],
    data: {}
};

export const planets = (
    state: Planets = INITIAL_STATE,
    action: AppAction
) => {
    switch (action.type) {
        case 'GetPlanetStart':
            return {
                ...state,
                loadingItems: state.loadingItems.concat(action.payload)
            }
        case 'GetPlanetSuccess':
            return {
                ...state,
                data: {
                    ...state.data,
                    [action.payload.url]: action.payload
                }
            }
        default:
            return state;
    }
}
