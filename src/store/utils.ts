export type ItemsHash<T> = {[url: string]: T}
export const arrayToHash = <T extends {url: string}>(items: T[]): ItemsHash<T> => {
    return items.reduce<ItemsHash<T>>((acc, item: T) => {
        acc[item.url] = item;
        return acc;
    }, {});
}