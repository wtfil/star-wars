import {ThunkAction, ThunkDispatch} from 'redux-thunk';

import * as actions from './actions';

export type AppAction =
    actions.SearchStart
    | actions.SearchSuccess
    | actions.SearchSkip
    | actions.SearchError
    | actions.GetPlanetStart
    | actions.GetPlanetSuccess
    | actions.GetPlanetError
    | actions.DuckSearchStart
    | actions.DuckSearchSuccess
    | actions.DuckSearchError
    | actions.GetFilmtStart
    | actions.GetFilmtSuccess
    | actions.GetFilmtError

export type ActionType<T, V = undefined> = {type: T} & (
    V extends Error ? {error: V} :
    V extends NonNullable<V> ? {payload: V} :
    {}
)

export interface Character {
    name: string;
    gender: 'male' | 'feemale';
    eye_color: string;
    birth_year: string;
    hair_color: string;
    homeworld: string;
    height: string;
    mass: string;
    species: string[];
    starships: string[];
    vehicles: string[];
    films: string[];
    url: string;
}

export interface Planet {
    name: string;
    rotation_period: string;
    orbital_period: string;
    diameter: string;
    climate: string;
    gravity: string;
    terrain: string;
    surface_water: string;
    population: string;
    residents: string[];
    films: string[];
    url: string;
}

export interface Film {
    characters: string[];
    director: string;
    episode_id: number;
    opening_crawl: string;
    planets: string[];
    producer: string;
    release_date: string;
    species: string[];
    starships: string[];
    title: string;
    url: string;
    vehicles: string[];
}

export interface CharacterPopuplated extends Character {
    homeworldPopuplated?: Planet;
    filmsPopulated: Array<Film | undefined>;
    image?: string;
}

export interface State {
    characters: {
        [url: string]: Character
    },
    planets: {
        loadingItems: string[];
        data: {
            [url: string]: Planet
        }
    },
    films: {
        loadingItems: string[];
        data: {
            [url: string]: Film
        }
    },
    search: {
        value: string,
        status: 'ready' | 'loading',
        history: string[]
    },
    images: {
        [name: string]: string
    }
}

export type AppCommand = ThunkAction<void, State, undefined, AppAction>
export type Dispatch = ThunkDispatch<State, undefined, AppAction>