import {stringify} from 'querystring';

import {Character, Planet, Film} from './types';

const API_HOST = 'https://swapi.co/api';
const DUCK_DUCK_GO_API_HOST = 'https://api.duckduckgo.com'

const callApi = async <T>(
    path: string,
    query?: {[key: string]: string}
): Promise<T> => {
    let url = path;
    if (query) {
        url += `?${stringify(query)}`
    }
    const res = await fetch(url);
    return res.json();
}

export interface SearchResponse {
    count: number;
    results: Character[]
}

export interface DuckDuckGoSearchResponce {
    Image: string;
}

export const search = (value: string) => callApi<SearchResponse>(`${API_HOST}/people`, {search: value})
export const getPlanet = (url: string) => callApi<Planet>(url);
export const getFilm = (url: string) => callApi<Film>(url);
export const duckSearch = (query: string) => callApi<DuckDuckGoSearchResponce>(
    DUCK_DUCK_GO_API_HOST,
    {format: 'json', q: query}
)