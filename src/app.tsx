import * as React from 'react';
import {Provider} from 'react-redux';

import {createStore} from './store';
import {SearchPageContainer} from './containers';

export const App: React.SFC = () => (
    <Provider store={createStore()}>
        <SearchPageContainer/>
    </Provider>
)