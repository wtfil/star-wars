import * as React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

import * as style from './character-card.css';
import {CharacterPopuplated, Film} from '../../store';

const DEFAULT_IMAGE = 'https://www.clipartmax.com/png/middle/31-313897_avatar-contact-default-starwars-user-yoda-icon-star-wars-yoda-silhouette-transparent.png';

export interface CharacterCardProps {
    character: CharacterPopuplated;
}
export const CharacterCard: React.SFC<CharacterCardProps> = props => (
    <Card className={style.root} raised>
        <CardMedia
            image={props.character.image || DEFAULT_IMAGE}
            className={style.media}
        />
        <CardHeader title={props.character.name}/>
        <CardContent className={style.content}>
            <Table>
                <TableBody>
                    <TableRow className={style.row} hover>
                        <TableCell>Eyes</TableCell>
                        <TableCell numeric>{props.character.eye_color}</TableCell>
                    </TableRow>
                    <TableRow className={style.row} hover>
                        <TableCell>Gender</TableCell>
                        <TableCell numeric>{props.character.gender}</TableCell>
                    </TableRow>
                    <TableRow className={style.row} hover>
                        <TableCell>Hair</TableCell>
                        <TableCell numeric>{props.character.hair_color}</TableCell>
                    </TableRow>
                    <TableRow className={style.row} hover>
                        <TableCell>Birth</TableCell>
                        <TableCell numeric>{props.character.birth_year}</TableCell>
                    </TableRow>
                    <TableRow className={style.row} hover>
                        <TableCell>Height</TableCell>
                        <TableCell numeric>{props.character.height}</TableCell>
                    </TableRow>
                    <TableRow className={style.row} hover>
                        <TableCell>Mass</TableCell>
                        <TableCell numeric>{props.character.mass}</TableCell>
                    </TableRow>
                    <TableRow className={style.row} hover>
                        <TableCell>Films</TableCell>
                        <TableCell numeric>
                            {props.character.filmsPopulated.filter(Boolean).map((item, index, arr) => (
                                <React.Fragment key={index}>
                                    {item!.title}
                                    {index !== arr.length -1 &&
                                        <>;<br/></>
                                    }
                                </React.Fragment>
                            ))}
                        </TableCell>
                    </TableRow>
                    {props.character.homeworldPopuplated &&
                        <TableRow className={style.row} hover>
                            <TableCell>Home world</TableCell>
                            <TableCell numeric>{props.character.homeworldPopuplated.name}</TableCell>
                        </TableRow>
                    }
                </TableBody>
            </Table>
        </CardContent>
    </Card>
)