import * as React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

import * as style from './search-status.css';

export interface SearchStatusProps {
    query: string;
    items: any[];
    status: 'loading' | 'ready'
}
export const SearchStatus: React.SFC<SearchStatusProps> = props => {
    if (!props.query.trim()) {
        return null;
    }
    if (props.status === 'ready' && !props.items.length) {
        return (
            <div className={style.root}>
                <Typography
                    variant="h5"
                    align="center"
                >
                    There are not results for "{props.query}"
            </Typography>
            </div>
        )
    }
    if (props.status === 'loading' && !props.items.length) {
        return (
            <div className={style.root}>
                <CircularProgress/>
            </div>
        )
    }
    return null;
}
