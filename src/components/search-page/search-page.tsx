import * as React from 'react';
import {Header, HeaderProps} from '../header';
import {SearchStatus} from '../search-status';
import {CharacterList} from '../character-list';
import {CharacterPopuplated} from '../../store';

export interface SearchPageProps {
    characters: CharacterPopuplated[];
    query: string;
    onChange: HeaderProps['onChange']
    status: 'loading' | 'ready'
}

export const SearchPage: React.SFC<SearchPageProps> = props => (
    <div>
        <Header
            value={props.query}
            onChange={props.onChange}
        />
        <SearchStatus
            query={props.query}
            status={props.status}
            items={props.characters}
        />
        <CharacterList
            characters={props.characters}
        />
    </div>
)