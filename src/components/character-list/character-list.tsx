import * as React from 'react';
import Grid from '@material-ui/core/Grid';

import {CharacterPopuplated} from '../../store';
import {CharacterCard} from '../character-card';

import * as style from './character-list.css';

export interface CharacterListProps {
    characters: CharacterPopuplated[];
}

export const CharacterList: React.SFC<CharacterListProps> = props => (
    <Grid container spacing={16} className={style.root}>
        {props.characters.map((item, index) => (
            <Grid item key={index}>
                <CharacterCard character={item} />
            </Grid>
        ))}
    </Grid>
)
