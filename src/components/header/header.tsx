import * as React from 'react';
import * as debounce from 'debounce';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';

import * as style from './header.css';

export interface HeaderProps {
    value?: string;
    onChange?: (e: {value: string}) => void;
}
export interface HeaderState {
    value: string;
}

export class Header extends React.Component<HeaderProps, HeaderState> {
    public state = {
        value: this.props.value || ''
    }
    public componentWillUnmount() {
        this.callOnChange.clear();
    }
    public render() {
        const {value} = this.state;
        return (
            <AppBar className={style.root}>
                <Toolbar variant="dense">
                    <Typography variant="h6" color="inherit" noWrap>
                        Star Wars
                    </Typography>
                    <div className={style.grow} />
                    <div className={style.searchWrap}>
                        <div className={style.search}>

                            <SearchIcon />
                        </div>
                        <InputBase
                            classes={{
                                root: style.inputWrap,
                                input: style.input
                            }}
                            spellCheck={false}
                            placeholder="Search…"
                            onChange={this.onChange}
                            value={value}
                        />
                    </div>
                </Toolbar>
            </AppBar>

        )
    }
    private onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {value} = e.target;
        this.setState({value});
        this.callOnChange(value);
    }
    private callOnChange = debounce((value: string) => {
        if (this.props.onChange) {
            this.props.onChange({value});
        }
    }, 300)
}
