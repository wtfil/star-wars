import {connect} from 'react-redux';
import {SearchPage, SearchPageProps} from '../components';
import {State, search, Dispatch} from '../store';

const selectCharacters = (state: State) => {
    const query = state.search.value.trim();
    if (!query) {
        return [];
    }
    const re = new RegExp(query, 'i')
    return Object.values(state.characters)
        .filter(item => re.test(item.name))
        .map(item => ({
            ...item,
            homeworldPopuplated: state.planets.data[item.homeworld],
            filmsPopulated: item.films.map(film =>
                state.films.data[film]
            ),
            image: state.images[item.name]
        }))
}
const selectSearchQuery = (state: State) => state.search.value;
const selectStatus = (state: State) => state.search.status;

export const SearchPageContainer: React.ComponentClass = connect<
    Pick<SearchPageProps, 'characters' | 'query' | 'status'>,
    Pick<SearchPageProps, 'onChange'>,
    {},
    State
>(
    state => ({
        characters: selectCharacters(state),
        query: selectSearchQuery(state),
        status: selectStatus(state)
    }),
    (dispatch: Dispatch) => ({
        onChange: e => dispatch(search(e.value))
    })
)(SearchPage);
