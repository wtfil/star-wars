import * as React from 'react';
import {render} from 'react-dom';
import {App} from './app';

const node = document.createElement('div');
document.body.appendChild(node);

render(React.createElement(App), node);